# Hi there, I'm [BinaryBytecode!](https://github.com/binarybytecode) 👋.  <img src="https://raw.githubusercontent.com/8bithemant/8bithemant/master/svg/pronouns/hehim.svg" >

<br />

## ⚙️ &nbsp;GitHub Analytics

<p align="center">
<a href="https://github.com/BinaryBytecode">
  <img height="180em" src="https://github-readme-stats-eight-theta.vercel.app/api?username=BinaryBytecode&show_icons=true&theme=vue-dark&include_all_commits=true&count_private=true" />
  <img height="180em" src="https://github-readme-stats-eight-theta.vercel.app/api/top-langs/?username=BinaryBytecode&layout=compact&exclude_lang=java+r&theme=vue-dark" />
</a>
</p>


## 🎉 Skills 🎉
 - Web technologies: HTML, CSS, PHP, JS
 - Databases: MySQL, MariaDB, SQLite
 - Languages: Python, nodejs, lua (and a bit of golang, C, C++)
